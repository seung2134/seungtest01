# OnBoarding mini project

## Features
- *ngIf
- *ngFor (check out the index property for your list numbers)
- Add a Subject or Behaviour Subject that is subscribed to in another component - (https://rxjs.dev/guide/subject)
- Unix times
- A Custom Pipe
- A Custom Directive
- An angular pipe (https://angular.io/guide/pipes)
- Dialog/Modal (PrimeNG dependant)
- ngStyle/ngClass Directive
- Route Guard
- Pagination / Watch Query 
- @Input/@Output properties
- @ViewChild
- Search Functionality
- GQL Mutation Functionality (Doesn’t have to persist since we don’t have a database setup)

## Usage

### Env Variabl

### Run

```
# Run frontend 
cd frontend 
npm start

# Run backend backend
cd backend
npm start
```
