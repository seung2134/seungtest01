import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginMsgService {

  private subject = new Subject<string>();
  constructor() { }

  SendOnLogin(message: string) {
    this.subject.next(message);
  }
  receivedOnLogin(): Observable<string> {
    return this.subject.asObservable();
  }
}
