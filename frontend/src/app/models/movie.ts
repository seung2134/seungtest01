export interface Movie {
  id: Number;
  title: String;
  year: Number;
  runtime: String;
  genres: [String];
  director: String;
  plot: String;
  posterUrl: String;
}
