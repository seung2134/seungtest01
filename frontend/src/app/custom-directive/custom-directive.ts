import { Directive,Renderer2,OnInit,HostListener, ElementRef } from "@angular/core";

@Directive({
    selector:'[appBasicHighlight]'
})
export class appBasicHighlight implements OnInit{
    constructor(private elRef:ElementRef, private renderer:Renderer2){

    }
    ngOnInit(): void {
    }
    @HostListener('mouseenter') mouseover(eventData:Event){
        this.renderer.setStyle(this.elRef.nativeElement,'background-color','LightSlateGray');
    }
    @HostListener('mouseleave') mouseleave(eventData:Event){
        this.renderer.setStyle(this.elRef.nativeElement,'background-color','transparent');
    }


}
