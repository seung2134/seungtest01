import { Component } from '@angular/core';
import { MenuItem } from 'primeng-lts/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'frontend';

  items: MenuItem[];
  activeItem: MenuItem;

  ngOnInit() {

    this.items = [
      { label: 'Home', routerLink: '/', icon: 'pi pi-fw pi-home' },
      { label: 'Movies', routerLink: 'movies', icon: 'pi pi-database' },
    ];

    this.activeItem = this.items[0];

  }
}
