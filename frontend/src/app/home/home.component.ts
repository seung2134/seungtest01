import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng-lts/api';
import { LoginMsgService } from '../login-msg.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [MessageService]
})
export class HomeComponent implements OnInit {
  onLogin: boolean = false;

  constructor(private router: Router, private authService: AuthService, private messageService: MessageService, private loginMsgService: LoginMsgService
  ) { }

  ngOnInit(): void {
    this.onLogin = this.authService.loggedIn
  }

  handleChange(e) {
    let isChecked = e.checked;
    if (isChecked) {
      this.sendMessage();
      this.authService.login();
      this.loginMsgService.SendOnLogin("You Login")

    } else {

      this.authService.logout();
      this.showFail();
    }

  }


  sendMessage() {
    this.loginMsgService.SendOnLogin("You Login")

  }


  showSuccess() {
    this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Log in' });
  }

  showFail() {
    this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Log out' });
  }


}
