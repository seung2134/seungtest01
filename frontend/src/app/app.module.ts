import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ApolloModule, Apollo } from 'apollo-angular';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { AppComponent } from './app.component';
import { MoviesComponent } from './movies/movies.component';
import { MovieComponent } from './movies/movie/movie.component';
import { convertUnix } from './convertUnix.pipe';
import { ReactiveFormsModule } from '@angular/forms';

// primeNG
import { ButtonModule } from 'primeng-lts/button';
import { DataViewModule } from 'primeng-lts/dataview';
import { CardModule } from 'primeng-lts/card';
import { FormsModule } from '@angular/forms';
import {DialogModule} from 'primeng-lts/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MessagesModule} from 'primeng-lts/messages';
import {MessageModule} from 'primeng-lts/message';
import {InputSwitchModule} from 'primeng-lts/inputswitch';
import {ToastModule} from 'primeng-lts/toast';
import {SelectButtonModule} from 'primeng-lts/selectbutton';
import {TabMenuModule} from 'primeng-lts/tabmenu';
import { Routes, RouterModule } from '@angular/router';
import { AuthService } from './auth.service';
import{MyGuardGuard} from './my-guard.guard';
import { TableModule } from 'primeng-lts/table';
import { appBasicHighlight } from './custom-directive/custom-directive';
import { HomeComponent } from './home/home.component';



const appRoutes: Routes = [
  {path:'',component:HomeComponent},
  { path: 'movies', component: MoviesComponent,canActivate:[MyGuardGuard],},
  { path: 'movies/:id', component: MovieComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    MoviesComponent,
    MovieComponent,
    convertUnix,
    appBasicHighlight,
    HomeComponent,


    
  ],
  imports: [
    BrowserModule,
    CardModule,
    ReactiveFormsModule,
    MessagesModule,
    MessageModule,
    ToastModule,
    ButtonModule,
    DataViewModule,
    ApolloModule,
    TabMenuModule,
    HttpLinkModule,
    HttpClientModule,
    InputSwitchModule,
    FormsModule,
    DialogModule,
    BrowserAnimationsModule,
    SelectButtonModule,
    RouterModule.forRoot(appRoutes),
    TableModule,
  ],
  providers: [ MyGuardGuard,AuthService],
  bootstrap: [AppComponent],
  exports:[RouterModule]
})
export class AppModule {
  constructor(apollo: Apollo, httpLink: HttpLink) {
    apollo.create({
      link: httpLink.create({ uri: 'http://localhost:4000/graphql' }),
      cache: new InMemoryCache(),
    });
  }
}
