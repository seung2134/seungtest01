import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Movie } from '../../models/movie';
import { Apollo } from 'apollo-angular';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { PrimeNGConfig } from 'primeng-lts/api';
import { MovieService } from '../movie.service';
import { MessageService } from 'primeng-lts/api';
import * as moment from 'moment';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css'],
  providers: [MessageService]
})

export class MovieComponent implements OnInit {
  movie: Movie | undefined;
  movies: [Movie] | undefined;
  movieId = '';
  isLongMovie = false;
  isEditing = false;
  movieTitle = '';
  movieURL = '';
  likeCount = 0;
  editYear: Date;
  displayModal: boolean = false;
  editedMovieDetail: Movie | undefined;
  @ViewChild('yearRef') yearElementRef: ElementRef;
  movieDetailEditForm: FormGroup;
  REGEX_NUMBERS_ONLY = /^[0-9]*$/;

  constructor(
    private activatedRouter: ActivatedRoute,
    private apollo: Apollo,
    private primengConfig: PrimeNGConfig,
    private router: Router,
    private messageService: MessageService,
    private movieService: MovieService,

  ) { }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
    this.movieId = this.activatedRouter.snapshot.params['id'];
    this.movieDetailEditForm = new FormGroup({
      runtime: new FormControl(null, [Validators.required, Validators.minLength(1), Validators.pattern(this.REGEX_NUMBERS_ONLY),
      ]),
      genres: new FormControl(null, [Validators.required, Validators.minLength(2)]),
      director: new FormControl(null, [Validators.required, Validators.minLength(2)]),
      plot: new FormControl(null, [Validators.required, Validators.minLength(15)]),
      year: new FormControl(null, Validators.required),
    })
    // Set input value
    this.movieService.getMovieById(+this.movieId).valueChanges.subscribe(({ data }) => {
      this.movie = data['getMovie'];
      if (this.movie?.runtime != undefined) {
        this.isLongMovie = +this.movie?.runtime > 120 ? true : false;
      }
    });

  }

  // Mutation query
  updateMovie() {
    this.movieService.updateMovie(this.editedMovieDetail).subscribe(({ data }) => {
      this.movie = data.updateMovie;
    });
  }


  deleteMovie() {
    this.movieService.deleteMovie(this.movieId)
      .subscribe(({ data }) => {
        this.movies = data.deleteMovie;
      });
  }


  onDeleteMovieItem() {
    this.deleteMovie();
    this.showSuccess();
    this.router.navigate(['/movies'])
    this.displayModal = false;
  }

  onToggleEditingMode() {
    this.isEditing = !this.isEditing;
    if (this.isEditing) {
      this.setPrefilledInput();
      setTimeout(() => {
        this.yearElementRef.nativeElement.focus();
      })
    }
  }

  setPrefilledInput() {
    this.movieDetailEditForm.patchValue({
      runtime: this.movie.runtime,
      genres: this.movie.genres,
      director: this.movie.director,
      plot: this.movie.plot,
      year: this.convertUnixToDate(this.movie.year)
    })

  }

  // Submit edited item info
  onSubmit(): void {
    const editValues = this.movieDetailEditForm.getRawValue();

    this.editedMovieDetail = {
      id: +this.movieId,
      title: this.movie?.title!,
      posterUrl: editValues.posterUrl!,
      genres: editValues.genres,
      year: this.convertDateToUnix(editValues.year),
      runtime: editValues.runtime,
      director: editValues.director,
      plot: editValues.plot,
    }

    this.isEditing = !this.isEditing;
    this.updateMovie();
  }
  showModalDialog() {
    this.displayModal = true;
  }
  // Toast Message
  showSuccess() {
    this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Message Content' });
  }

  showChange() {
    this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Change Movie Info' });
  }

  convertDateToUnix(date: string) {
    let convDate = +((new Date(date).getTime() / 1000).toFixed(0));
    return convDate;
  }

  convertUnixToDate(unixTimeStamp: any) {
    return moment.unix(unixTimeStamp).format("YYYY-MM-DD");
  }

}
