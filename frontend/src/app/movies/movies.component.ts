import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Apollo, Query, QueryRef } from 'apollo-angular';
import { Subscription } from 'rxjs';
import { Movie } from '../models/movie';
import { GetAllMoviesOutput, MovieService } from './movie.service';
import { LoginMsgService } from '../login-msg.service';


@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css'],
})
export class MoviesComponent implements OnInit, OnDestroy {
  first = 0;
  rows = 10;
  loading: boolean;
  message: string;
  allMovies: Movie[] = [];
  searchText: String = '';
  moviesQuery: QueryRef<GetAllMoviesOutput>;
  private querySubscription: Subscription;

  constructor(private apollo: Apollo, private loginMsgService: LoginMsgService, private movieService: MovieService
  ) { }

  ngOnInit(): void {
    this.fetchMovies();
  }


  fetchMovies() {
    if (!this.moviesQuery) {
      this.initMovieQuery();
      return;
    }

    this.moviesQuery.refetch();
  }

  initMovieQuery() {
    this.moviesQuery = this.movieService.getAllMovies();
    this.querySubscription = this.moviesQuery.valueChanges.subscribe((res) => {
      if (res.data) {
        this.loading = res.loading;
        // this.allMovies = res.data['getAllMovies'];
        this.allMovies = res.data.getAllMovies

      }
    })
  }

  ngOnDestroy() {
    this.querySubscription.unsubscribe();
  }

  next() {
    this.first = this.first + this.rows;
  }

  prev() {
    this.first = this.first - this.rows;
  }

  reset() {
    this.first = 0;
  }

  isLastPage(): boolean {
    return this.allMovies
      ? this.first === this.allMovies.length - this.rows
      : true;
  }

  isFirstPage(): boolean {
    return this.allMovies ? this.first === 0 : true;
  }
  onSearchTextEntered(searchValue: string) {
    this.searchText = searchValue;
  }


}
