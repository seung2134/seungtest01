import { Injectable } from '@angular/core';
import { Apollo, Query, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';
import { Movie } from '../models/movie';

export interface GetAllMoviesOutput {
  getAllMovies: Movie[];
}
export interface GetMovieByIdOutput {
  getMovie: Movie;
}
const getAllMovies = gql`
  query GetAllMovies{
    getAllMovies {
      id
      title
      year
      runtime
      genres
      director
      plot
      posterUrl
    }
  }
`;
const getMovieById = gql`
  query GetMovie($getMovieId: Int) {
    getMovie(id: $getMovieId) {
      id
      title
      year
      runtime
      genres
      director
      plot
      posterUrl
    }
  }
`;
const deleteMovie = gql`
  mutation DeleteMovie(
    $deleteMovieId: Int
  ) {
    deleteMovie(id: $deleteMovieId) {
      id
      title
      runtime
      director
      plot
      genres
      year
      posterUrl
    }
  }
`;
const updateMovieDetail = gql`
  mutation UpdateMovie(
    $genres: [String]
    $director: String
    $plot: String 
    $runtime: String 
    $title: String 
    $updateMovieId: Int 
    $year: Int
  ) {
    updateMovie(
      genres: $genres 
      director: $director 
      plot: $plot 
      runtime: $runtime 
      title: $title 
      id: $updateMovieId 
      year: $year
    ) {
      id
      title
      year
      runtime
      genres
      director
      plot
      posterUrl
    }
  }
`;
@Injectable({
  providedIn: 'root'
})
export class MovieService {
  constructor(private apollo: Apollo) { }

  getAllMovies(): QueryRef<GetAllMoviesOutput> {
    return this.apollo.watchQuery<GetAllMoviesOutput>({
      query: getAllMovies,
      fetchPolicy: 'cache-and-network'
    })
  }



  getMovieById(movieId: any): QueryRef<GetMovieByIdOutput> {
    return this.apollo
      .watchQuery<GetMovieByIdOutput>({
        query: getMovieById,
        variables: {
          getMovieId: +movieId,
        },
      })

  }

  updateMovie(editedMovieDetail: any) {
    return this.apollo
      .mutate<any>({
        mutation: updateMovieDetail,
        variables: {
          updateMovieId: editedMovieDetail?.id,
          title: editedMovieDetail?.title,
          runtime: editedMovieDetail?.runtime,
          director: editedMovieDetail?.director,
          plot: editedMovieDetail?.plot,
          year: +editedMovieDetail?.year,
          genres: editedMovieDetail?.genres
        },
      })
  }

  deleteMovie(movieId: any) {
    return this.apollo
      .mutate<any>({
        mutation: deleteMovie,
        variables: {
          deleteMovieId: +movieId,
        },
      })



  }
}