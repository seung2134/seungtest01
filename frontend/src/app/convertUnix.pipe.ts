import { Pipe, PipeTransform } from '@angular/core';

// get the Unix time stamp 
// convert and return MM/YEAR data.

@Pipe({
  name: 'customUnixConvert',
})
export class convertUnix implements PipeTransform {
  transform(value: any) {
    const unixTimestamp = value;
    const milliseconds = unixTimestamp * 1000;
    const dateObject = new Date(milliseconds);
    return dateObject.toLocaleString('en-US', {
      year: 'numeric',
      month: 'long',
    });
  }
}
