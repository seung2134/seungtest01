// const { gql } = require("apollo-server-express");
import { gql } from "apollo-server-express";

const typeDefs = gql`
  type Query {
    getAllMovies: [Movie!]
    getMovie(id: Int): Movie!
  }
  type Mutation {
    updateMovie(
      id: Int
      title: String
      runtime: String
      director: String
      plot: String
      year:Int
      genres:[String]
    ): Movie
    deleteMovie(id: Int): [Movie!]
  }

  type Movie {
    id: Int!
    title: String
    year: Int
    runtime: String
    genres: [String]
    director: String
    plot: String
    posterUrl: String
  }


`;

export default typeDefs;
