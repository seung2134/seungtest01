import database from "../movieDB.js";

let movies = database.movies;
let targetMovie;
const resolvers = {
  Query: {
    getAllMovies() {
      return movies;
    },
    getMovie(roots = null, {id}) {
      return movies.find(movie=>movie.id==id);
    },
  },
  Mutation: {
    updateMovie(root = null, {id,title,runtime,director,plot,genres,year}) {
      targetMovie= movies.find(movie=>movie.id ==id);
      targetMovie.title = title;
      targetMovie.runtime= runtime;
      targetMovie.director=director;
      targetMovie.plot = plot;
      targetMovie.genres=genres;
      targetMovie.year=year;
      
      return targetMovie;


    },
    deleteMovie(roots = null, {id}) {      
      movies= movies.filter((movie) => {
        if(movie.id!==id){
          return movie
        }
      });
      return movies;
    },
  },
};

export default resolvers;
