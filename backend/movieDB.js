let movies = [
  {
    id: 1,
    title: "The Cotton Club",
    year: 443037600,
    runtime: "127",
    genres: ["Crime", "Drama", "Music"],
    director: "Francis Ford Coppola",
    actors: "Richard Gere, Gregory Hines, Diane Lane, Lonette McKee",
    plot: "The Cotton Club was a famous night club in Harlem. The story follows the people that visited the club, those that ran it, and is peppered with the Jazz music that made it so famous.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTU5ODAyNzA4OV5BMl5BanBnXkFtZTcwNzYwNTIzNA@@._V1_SX300.jpg",
  },

  {
    id: 2,
    title: "Crocodile Dundee",
    year: 511293600,
    runtime: "97",
    genres: ["Adventure", "Comedy"],
    director: "Peter Faiman",
    actors: "Paul Hogan, Linda Kozlowski, John Meillon, David Gulpilil",
    plot: "An American reporter goes to the Australian outback to meet an eccentric crocodile poacher and invites him to New York City.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTg0MTU1MTg4NF5BMl5BanBnXkFtZTgwMDgzNzYxMTE@._V1_SX300.jpg",
  },

  {
    id: 3,
    title: "Ratatouille",
    year: 1173978000,
    runtime: "111",
    genres: ["Animation", "Comedy", "Family"],
    director: "Brad Bird, Jan Pinkava",
    actors: "Patton Oswalt, Ian Holm, Lou Romano, Brian Dennehy",
    plot: "A rat who can cook makes an unusual alliance with a young kitchen worker at a famous restaurant.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTMzODU0NTkxMF5BMl5BanBnXkFtZTcwMjQ4MzMzMw@@._V1_SX300.jpg",
  },
  {
    id: 4,
    title: "City of God",
    year: 1016215200,
    runtime: "130",
    genres: ["Crime", "Drama"],
    director: "Fernando Meirelles, Kátia Lund",
    actors:
      "Alexandre Rodrigues, Leandro Firmino, Phellipe Haagensen, Douglas Silva",
    plot: "Two boys growing up in a violent neighborhood of Rio de Janeiro take different paths: one becomes a photographer, the other a drug dealer.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMjA4ODQ3ODkzNV5BMl5BanBnXkFtZTYwOTc4NDI3._V1_SX300.jpg",
  },

  {
    id: 5,
    title: "Stardust",
    year: 1181926800,
    runtime: "127",
    genres: ["Adventure", "Family", "Fantasy"],
    director: "Matthew Vaughn",
    actors: "Ian McKellen, Bimbo Hart, Alastair MacIntosh, David Kelly",
    plot: "In a countryside town bordering on a magical land, a young man makes a promise to his beloved that he'll retrieve a fallen star by venturing into the magical realm.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMjkyMTE1OTYwNF5BMl5BanBnXkFtZTcwMDIxODYzMw@@._V1_SX300.jpg",
  },
  {
    id: 6,
    title: "Apocalypto",
    year: 1150477200,
    runtime: "139",
    genres: ["Action", "Adventure", "Drama"],
    director: "Mel Gibson",
    actors:
      "Rudy Youngblood, Dalia Hernández, Jonathan Brewer, Morris Birdyellowhead",
    plot: "As the Mayan kingdom faces its decline, the rulers insist the key to prosperity is to build more temples and offer human sacrifices. Jaguar Paw, a young man captured for sacrifice, flees to avoid his fate.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BNTM1NjYyNTY5OV5BMl5BanBnXkFtZTcwMjgwNTMzMQ@@._V1_SX300.jpg",
  },

  {
    id: 7,
    title: "No Country for Old Men",
    year: 1190048400,
    runtime: "122",
    genres: ["Crime", "Drama", "Thriller"],
    director: "Ethan Coen, Joel Coen",
    actors: "Tommy Lee Jones, Javier Bardem, Josh Brolin, Woody Harrelson",
    plot: "Violence and mayhem ensue after a hunter stumbles upon a drug deal gone wrong and more than two million dollars in cash near the Rio Grande.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMjA5Njk3MjM4OV5BMl5BanBnXkFtZTcwMTc5MTE1MQ@@._V1_SX300.jpg",
  },

  {
    id: 8,
    title: "Black Swan",
    year: 1284829200,
    runtime: "108",
    genres: ["Drama", "Thriller"],
    director: "Darren Aronofsky",
    actors: "Natalie Portman, Mila Kunis, Vincent Cassel, Barbara Hershey",
    plot: 'A committed dancer wins the lead role in a production of Tchaikovsky\'s "Swan Lake" only to find herself struggling to maintain her sanity.',
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BNzY2NzI4OTE5MF5BMl5BanBnXkFtZTcwMjMyNDY4Mw@@._V1_SX300.jpg",
  },
  {
    id: 9,
    title: "Inception",
    year: 1287507600,
    runtime: "148",
    genres: ["Action", "Adventure", "Sci-Fi"],
    director: "Christopher Nolan",
    actors: "Leonardo DiCaprio, Joseph Gordon-Levitt, Ellen Page, Tom Hardy",
    plot: "A thief, who steals corporate secrets through use of dream-sharing technology, is given the inverse task of planting an idea into the mind of a CEO.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_SX300.jpg",
  },

  {
    id: 10,
    title: "The Silence of the Lambs",
    year: 687891600,
    runtime: "118",
    genres: ["Crime", "Drama", "Thriller"],
    director: "Jonathan Demme",
    actors:
      "Jodie Foster, Lawrence A. Bonney, Kasi Lemmons, Lawrence T. Wrentz",
    plot: "A young F.B.I. cadet must confide in an incarcerated and manipulative killer to receive his help on catching another serial killer who skins his victims.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ2NzkzMDI4OF5BMl5BanBnXkFtZTcwMDA0NzE1NA@@._V1_SX300.jpg",
  },

  {
    id: 11,
    title: "Pulp Fiction",
    year: 782586000,
    runtime: "154",
    genres: ["Crime", "Drama"],
    director: "Quentin Tarantino",
    actors: "Tim Roth, Amanda Plummer, Laura Lovelace, John Travolta",
    plot: "The lives of two mob hit men, a boxer, a gangster's wife, and a pair of diner bandits intertwine in four tales of violence and redemption.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTkxMTA5OTAzMl5BMl5BanBnXkFtZTgwNjA5MDc3NjE@._V1_SX300.jpg",
  },

  {
    id: 12,
    title: "Shutter Island",
    year: 1292781600,
    runtime: "138",
    genres: ["Mystery", "Thriller"],
    director: "Martin Scorsese",
    actors: "Leonardo DiCaprio, Mark Ruffalo, Ben Kingsley, Max von Sydow",
    plot: "In 1954, a U.S. marshal investigates the disappearance of a murderess who escaped from a hospital for the criminally insane.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTMxMTIyNzMxMV5BMl5BanBnXkFtZTcwOTc4OTI3Mg@@._V1_SX300.jpg",
  },

  {
    id: 13,
    title: "Moneyball",
    year: 1295460000,
    runtime: "133",
    genres: ["Biography", "Drama", "Sport"],
    director: "Bennett Miller",
    actors: "Brad Pitt, Jonah Hill, Philip Seymour Hoffman, Robin Wright",
    plot: "Oakland A's general manager Billy Beane's successful attempt to assemble a baseball team on a lean budget by employing computer-generated analysis to acquire new players.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMjAxOTU3Mzc1M15BMl5BanBnXkFtZTcwMzk1ODUzNg@@._V1_SX300.jpg",
  },
  {
    id: 14,
    title: "The Hangover",
    year: 1235152800,
    runtime: "100",
    genres: ["Comedy"],
    director: "Todd Phillips",
    actors: "Bradley Cooper, Ed Helms, Zach Galifianakis, Justin Bartha",
    plot: "Three buddies wake up from a bachelor party in Las Vegas, with no memory of the previous night and the bachelor missing. They make their way around the city in order to find their friend before his wedding.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTU1MDA1MTYwMF5BMl5BanBnXkFtZTcwMDcxMzA1Mg@@._V1_SX300.jpg",
  },
  {
    id: 15,
    title: "The Great Beauty",
    year: 1361383200,
    runtime: "141",
    genres: ["Drama"],
    director: "Paolo Sorrentino",
    actors: "Toni Servillo, Carlo Verdone, Sabrina Ferilli, Carlo Buccirosso",
    plot: "Jep Gambardella has seduced his way through the lavish nightlife of Rome for decades, but after his 65th birthday and a shock from the past, Jep looks past the nightclubs and parties to find a timeless landscape of absurd, exquisite beauty.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ0ODg1OTQ2Nl5BMl5BanBnXkFtZTgwNTc2MDY1MDE@._V1_SX300.jpg",
  },

  {
    id: 16,
    title: "Mary and Max",
    year: 1237568400,
    runtime: "92",
    genres: ["Animation", "Comedy", "Drama"],
    director: "Adam Elliot",
    actors: "Toni Collette, Philip Seymour Hoffman, Barry Humphries, Eric Bana",
    plot: "A tale of friendship between two unlikely pen pals: Mary, a lonely, eight-year-old girl living in the suburbs of Melbourne, and Max, a forty-four-year old, severely obese man living in New York.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ1NDIyNTA1Nl5BMl5BanBnXkFtZTcwMjc2Njk3OA@@._V1_SX300.jpg",
  },
  {
    id: 17,
    title: "Flight",
    year: 1337533200,
    runtime: "138",
    genres: ["Drama", "Thriller"],
    director: "Robert Zemeckis",
    actors:
      "Nadine Velazquez, Denzel Washington, Carter Cabassa, Adam C. Edwards",
    plot: "An airline pilot saves almost all his passengers on his malfunctioning airliner which eventually crashed, but an investigation into the accident reveals something troubling.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTUxMjI1OTMxNl5BMl5BanBnXkFtZTcwNjc3NTY1OA@@._V1_SX300.jpg",
  },
  {
    id: 18,
    title: "One Flew Over the Cuckoo's Nest",
    year: 169837200,
    runtime: "133",
    genres: ["Drama"],
    director: "Milos Forman",
    actors: "Michael Berryman, Peter Brocco, Dean R. Brooks, Alonzo Brown",
    plot: "A criminal pleads insanity after getting into trouble again and once in the mental institution rebels against the oppressive nurse and rallies up the scared patients.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BYmJkODkwOTItZThjZC00MTE0LWIxNzQtYTM3MmQwMGI1OWFiXkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_SX300.jpg",
  },
  {
    id: 19,
    title: "Requiem for a Dream",
    year: 958928400,
    runtime: "102",
    genres: ["Drama"],
    director: "Darren Aronofsky",
    actors: "Ellen Burstyn, Jared Leto, Jennifer Connelly, Marlon Wayans",
    plot: "The drug-induced utopias of four Coney Island people are shattered when their addictions run deep.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTkzODMzODYwOF5BMl5BanBnXkFtZTcwODM2NjA2NQ@@._V1_SX300.jpg",
  },

  {
    id: 20,
    title: "The Artist",
    year: 1305997200,
    runtime: "100",
    genres: ["Comedy", "Drama", "Romance"],
    director: "Michel Hazanavicius",
    actors: "Jean Dujardin, Bérénice Bejo, John Goodman, James Cromwell",
    plot: "A silent movie star meets a young dancer, but the arrival of talking pictures sends their careers in opposite directions.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMzk0NzQxMTM0OV5BMl5BanBnXkFtZTcwMzU4MDYyNQ@@._V1_SX300.jpg",
  },
  {
    id: 21,
    title: "WALL·E",
    year: 1211389200,
    runtime: "98",
    genres: ["Animation", "Adventure", "Family"],
    director: "Andrew Stanton",
    actors: "Ben Burtt, Elissa Knight, Jeff Garlin, Fred Willard",
    plot: "In the distant future, a small waste-collecting robot inadvertently embarks on a space journey that will ultimately decide the fate of mankind.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTczOTA3MzY2N15BMl5BanBnXkFtZTcwOTYwNjE2MQ@@._V1_SX300.jpg",
  },
  {
    id: 22,
    title: "The Wolf of Wall Street",
    year: 1369242000,
    runtime: "180",
    genres: ["Biography", "Comedy", "Crime"],
    director: "Martin Scorsese",
    actors: "Leonardo DiCaprio, Jonah Hill, Margot Robbie, Matthew McConaughey",
    plot: "Based on the true story of Jordan Belfort, from his rise to a wealthy stock-broker living the high life to his fall involving crime, corruption and the federal government.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMjIxMjgxNTk0MF5BMl5BanBnXkFtZTgwNjIyOTg2MDE@._V1_SX300.jpg",
  },
  {
    id: 23,
    title: "Hellboy II: The Golden Army",
    year: 1208883600,
    runtime: "120",
    genres: ["Action", "Adventure", "Fantasy"],
    director: "Guillermo del Toro",
    actors: "Ron Perlman, Selma Blair, Doug Jones, John Alexander",
    plot: "The mythical world starts a rebellion against humanity in order to rule the Earth, so Hellboy and his team must save the world from the rebellious creatures.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMjA5NzgyMjc2Nl5BMl5BanBnXkFtZTcwOTU3MDI3MQ@@._V1_SX300.jpg",
  },

  {
    id: 24,
    title: "Santa man",
    year: 1145034000,
    runtime: "92",
    genres: ["Comedy"],
    director: "Eric Steven Stahl",
    actors: "Beau Bridges, Rosanna Arquette, Mathew Botuchis, Shiri Appleby",
    plot: "A 17-year-old boy buys mini-cameras and displays the footage online at I-see-you.com. The cash rolls in as the site becomes a major hit. Everyone seems to have fun until it all comes crashing down....",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTYwMDUzNzA5Nl5BMl5BanBnXkFtZTcwMjQ2Njk3MQ@@._V1_SX300.jpg",
  },
  {
    id: 25,
    title: "The Grand Budapest Hotel",
    year: 1394816400,
    runtime: "99",
    genres: ["Adventure", "Comedy", "Crime"],
    director: "Wes Anderson",
    actors: "Ralph Fiennes, F. Murray Abraham, Mathieu Amalric, Adrien Brody",
    plot: "The adventures of Gustave H, a legendary concierge at a famous hotel from the fictional Republic of Zubrowka between the first and second World Wars, and Zero Moustafa, the lobby boy who becomes his most trusted friend.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMzM5NjUxOTEyMl5BMl5BanBnXkFtZTgwNjEyMDM0MDE@._V1_SX300.jpg",
  },

  {
    id: 26,
    title: "Once Upon a Time in America",
    year: 448135200,
    runtime: "229",
    genres: ["Crime", "Drama"],
    director: "Sergio Leone",
    actors: "Robert De Niro, James Woods, Elizabeth McGovern, Joe Pesci",
    plot: "A former Prohibition-era Jewish gangster returns to the Lower East Side of Manhattan over thirty years later, where he once again must confront the ghosts and regrets of his old life.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMGFkNWI4MTMtNGQ0OC00MWVmLTk3MTktOGYxN2Y2YWVkZWE2XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SX300.jpg",
  },
  {
    id: 27,
    title: "Oblivion",
    year: 1363280400,
    runtime: "124",
    genres: ["Action", "Adventure", "Mystery"],
    director: "Joseph Kosinski",
    actors: "Tom Cruise, Morgan Freeman, Olga Kurylenko, Andrea Riseborough",
    plot: "A veteran assigned to extract Earth's remaining resources begins to question what he knows about his mission and himself.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTQwMDY0MTA4MF5BMl5BanBnXkFtZTcwNzI3MDgxOQ@@._V1_SX300.jpg",
  },
  {
    id: 28,
    title: "V for Vendetta",
    year: 1105725600,
    runtime: "132",
    genres: ["Action", "Drama", "Thriller"],
    director: "James McTeigue",
    actors: "Natalie Portman, Hugo Weaving, Stephen Rea, Stephen Fry",
    plot: 'In a future British tyranny, a shadowy freedom fighter, known only by the alias of "V", plots to overthrow it with the help of a young woman.',
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BOTI5ODc3NzExNV5BMl5BanBnXkFtZTcwNzYxNzQzMw@@._V1_SX300.jpg",
  },
  {
    id: 29,
    title: "Gattaca",
    year: 868899600,
    runtime: "106",
    genres: ["Drama", "Sci-Fi", "Thriller"],
    director: "Andrew Niccol",
    actors: "Ethan Hawke, Uma Thurman, Gore Vidal, Xander Berkeley",
    plot: "A genetically inferior man assumes the identity of a superior one in order to pursue his lifelong dream of space travel.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BNDQxOTc0MzMtZmRlOS00OWQ5LWI2ZDctOTAwNmMwOTYxYzlhXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg",
  },
  {
    id: 30,
    title: "Silver Linings Playbook",
    year: 1337014800,
    runtime: "122",
    genres: ["Comedy", "Drama", "Romance"],
    director: "David O. Russell",
    actors: "Bradley Cooper, Jennifer Lawrence, Robert De Niro, Jacki Weaver",
    plot: "After a stint in a mental institution, former teacher Pat Solitano moves back in with his parents and tries to reconcile with his ex-wife. Things get more challenging when Pat meets Tiffany, a mysterious girl with problems of her own.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTM2MTI5NzA3MF5BMl5BanBnXkFtZTcwODExNTc0OA@@._V1_SX300.jpg",
  },
  {
    id: 31,
    title: "Alice in Wonderland",
    year: 1287075600,
    runtime: "108",
    genres: ["Adventure", "Family", "Fantasy"],
    director: "Tim Burton",
    actors: "Johnny Depp, Mia Wasikowska, Helena Bonham Carter, Anne Hathaway",
    plot: "Nineteen-year-old Alice returns to the magical world from her childhood adventure, where she reunites with her old friends and learns of her true destiny: to end the Red Queen's reign of terror.",
    posterUrl:
      "https://images-na.ssl-images-amazon.com/images/M/MV5BMTMwNjAxMTc0Nl5BMl5BanBnXkFtZTcwODc3ODk5Mg@@._V1_SX300.jpg",
  },
];

export default { movies };
